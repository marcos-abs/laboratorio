<?php
 # *****
 # File: functions.php
 # Project: Calendar PHP 8
 # File Created: Wednesday, 27 January 2021 14:39:15
 # Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 # -----
 # Last Modified: Wednesday, 27 January 2021 17:01:29
 # Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 # -----
 # Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 # -----
 # Description:
 #              Biblioteca de funções
 # *****

function prevMonth($time) {
    return date('Y-m-d', strtotime("-1 month", $time));
}

function nextMonth($time) {
    return date('Y-m-d', strtotime("+1 month", $time));
}

function getMonthTime() {
    $monthTime = strtotime(date('Y-m-d'));
}

function get($x) {
    return stripslashes(htmlentities($_GET[$x]));
}

function getMonthTime() {
    $monthTime = strtotime(date('Y-m-1'));
    if(isset($_GET["month"])) {
        // TODO: parei aqui 1h04m27s https://youtu.be/O1nT0jvj11A?t=3867
    }
}
?>