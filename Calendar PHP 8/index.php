<?php
 # *****
 # File: index.php
 # Project: Calendar PHP 8
 # File Created: Wednesday, 27 January 2021 14:39:23
 # Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 # -----
 # Last Modified: Wednesday, 27 January 2021 16:29:18
 # Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 # -----
 # Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 # -----
 # Description:
 #              Página Principal.
 # *****

/* TODO: parei aqui 53m37s */
require_once("functions.php");

echo "<link rel='stylesheet' href='style.css'>";

echo "<header>";
echo "    <a href='#'>Anterior</a>";
echo "    <h1>Mês</h1>";
echo "    <a href='#'>Próximo</a>";
echo "</header>";
/*
echo "<table>";
echo "    <thead>";
echo "        <tr>";
echo "            <th>DOM</th>";
echo "            <th>SEG</th>";
echo "            <th>TER</th>";
echo "            <th>QUA</th>";
echo "            <th>QUI</th>";
echo "            <th>SEX</th>";
echo "            <th>SAB</th>";
echo "        </tr>";
echo "    </thead>";
echo "    <tbody>";
echo "        <tr>";
echo "            <td>27</td>";
echo "            <td>28</td>";
echo "            <td>29</td>";
echo "            <td>30</td>";
echo "            <td>31</td>";
echo "            <td>01</td>";
echo "            <td>02</td>";
echo "        </tr>";
echo "    </tbody>";
echo "</table>";
*/