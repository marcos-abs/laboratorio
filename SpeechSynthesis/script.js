/**
 * File: script.js
 * Project: SpeechSynthesis
 * File Created: Wednesday, 07 October 2020 16:04:32
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Wednesday, 07 October 2020 16:35:59
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2020 All rights reserved, Marcant Tecnologia
 * -----
 */
const utterance = new SpeechSynthesisUtterance();

utterance.lang = "pt-BR";
utterance.rate = 1.5; // velocidade da fala
//utterance.pitch = 2; // altera a voz

function speak() {
	speechSynthesis.speak(utterance);
}

function stop() {
  speechSynthesis.cancel();
}

function setText(event) {
	utterance.text = event.target.innerText;
}
