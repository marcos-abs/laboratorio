/**
 * *****
 * File: prototype.js
 * Project: Criando Objetos
 * File Created: Sunday, 25 July 2021 15:31:04
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 25 July 2021 15:31:26
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description: JavaScript - Maneiras de criar objetos - Root #03
 *              https://www.youtube.com/watch?v=X6klpqubyBw
 * *****
 */

// Prototype
