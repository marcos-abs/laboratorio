/**
 * *****
 * File: factoryClass.js
 * Project: Criando Objetos
 * File Created: Sunday, 25 July 2021 14:59:31
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 25 July 2021 15:29:19
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description: JavaScript - Maneiras de criar objetos - Root #03
 *              https://www.youtube.com/watch?v=X6klpqubyBw
 * *****
 */

// Factory
function createMicrofone(color = "black") {
    let isOn = true;
    function toggleOnOff() {
        if (isOn) {
            console.log("desligar");
        } else {
            console.log("ligar");
        }
        isOn = !isOn;
}
    return {
        color, // (short hand) outra forma de escrever isso: color: color,
        toggleOnOff //(short hand) outra forma de escrever isso: toggleOnOff: toggleOnOff,
    };
}

const microfoneBlack = createMicrofone();
const microfoneWhite = createMicrofone("white");
console.log('microfoneBlack: ', microfoneBlack.color);
console.log('microfoneWhite: ', microfoneWhite.color);

