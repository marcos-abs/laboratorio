/*
 * *****
 * File: parei aqui.md
 * Project: Criando Objetos
 * File Created: Sunday, 25 July 2021 15:32:26
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 25 July 2021 15:33:35
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description: JavaScript - Maneiras de criar objetos - Root #03
 *              https://www.youtube.com/watch?v=X6klpqubyBw
 * *****
 */

''' Fonte do vídeo está em: https://www.youtube.com/watch?v=X6klpqubyBw
''' Parei em 21m50s de 38m01s
