/**
 * *****
 * File: class.js
 * Project: Criando Objetos
 * File Created: Sunday, 25 July 2021 12:57:10
 * Author: Marcos Antônio Barbosa de Souza (marcantech@uol.com.br)
 * -----
 * Last Modified: Sunday, 25 July 2021 14:58:42
 * Modified By: Marcos Antônio Barbosa de Souza (<marcantech@uol.com.br>)
 * -----
 * Copyright (c) 2019 - 2021 All rights reserved, Marcant Tecnologia
 * -----
 * Description: JavaScript - Maneiras de criar objetos - Root #03
 *              https://www.youtube.com/watch?v=X6klpqubyBw
 * *****
 */

// Object Literal
const microfone = {
    color: "black",
    isOn: true,
    toggleOnOff: function () { // também pode ser uma arrow function, ex: toggleOnOff: () => {} ou ainda um "short hand property", ex: toggleOnOff() {}
        if (microfone.isOn) {
            console.log("desligar");
        } else {
            console.log("ligar");
        }
        microfone.isOn = !microfone.isOn;
    },
};

console.log(microfone.color);
microfone.toggleOnOff();
microfone.toggleOnOff();

